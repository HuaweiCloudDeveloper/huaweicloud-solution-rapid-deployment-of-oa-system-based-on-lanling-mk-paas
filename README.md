[TOC]

**解决方案介绍**
===============
该解决方案能帮助用户快速部署蓝凌MK数字化办公平台，MK-PaaS是企业的技术中台，以企业门户为统一入口，采用平台产品支撑+平台应用商城的模式，将产品包、内容包、服务包、ISV解决方案封装为平台应用，构建全新的企业OA生态。MK-PaaS采用微服务架构，部署可拆可合，可以云端部署也可以私有化部署，提供统一门户、智能机器人、智能搜索、埋点和用户分析、运维监控、流程引擎、建模引擎等基座能力。

解决方案实践详情页面：[https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-oa-system-based-on-lanling-mk-paas.html](https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-oa-system-based-on-lanling-mk-paas.html)

**架构图**
---------------
![方案架构](./document/rapid-deployment-of-oa-system-based-on-lanling-mk-paas.png)

**架构描述**
---------------
该解决方案会部署如下资源：

1、创建一台弹性云服务器，用于部署蓝凌MK数字化办公平台。

2、创建一个弹性公网IP，用于提供服务器访问公网和被公网访问能力。

3、创建安全组，通过配置安全组规则，为弹性云服务器提供安全防护。


**组织结构**
---------------

``` lua
huaweicloud-solution-rapid-deployment-of-oa-system-based-on-lanling-mk-paas
├── rapid-deployment-of-oa-system-based-on-lanling-mk-paas.tf.json -- 资源编排模板
├── userdate
    ├── install-mk-paas.sh  -- 脚本配置文件
```
**开始使用**
---------------
***访问网站和初始化***

步骤 1	访问MKPaaS平台。软件启动大约需30分钟，之后，复制图中所看到的访问地址，在浏览器打开。

图1 访问网址
  ![访问网址](./document/readme-image-001.png)
步骤 2	初始化系统。配置三元管理，依次配置账号sysadmin、secadmin、secauditor密码。

图2 三元管理
  ![三元管理](./document/readme-image-002.jpg)
图3 设置系统管理员密码
  ![设置系统管理员密码](./document/readme-image-003.jpg)
图4 设置安全保密员密码
  ![设置安全保密员密码](./document/readme-image-004.jpg)
图5 设置安全审计员密码
  ![设置安全审计员密码](./document/readme-image-005.jpg)
步骤 3	点击“完成”后，使用sysadmin账号登录系统。

图6 登录
  ![登录](./document/readme-image-006.jpg)
步骤 4	系统初始化，按下图点击对应的选项，然后执行。

图7 系统初始化
  ![系统初始化](./document/readme-image-007.jpg)
步骤 5	修改附件存储目录，填写图中所示目录路径。

图8 新建目录
  ![新建目录](./document/readme-image-008.jpg)
图9 保存新建目录
 ![保存新建目录](./document/readme-image-009.jpg)
步骤 6	查看监控平台。

图10 监控平台
 ![监控平台](./document/readme-image-010.jpg)
步骤 7	查看在线帮助文档。

图11 在线帮助
  ![在线帮助](./document/readme-image-011.jpg)
图12 帮助文档
  ![帮助文档](./document/readme-image-012.jpg)
***业务用户使用***

步骤 1	使用安全保密员账号secadmin登录系统，然后新建人员。

图13 新建人员
  ![新建人员](./document/readme-image-013.jpg)
图14 保存新建人员
  ![保存新建人员](./document/readme-image-014.png)
步骤 2	新建人员分配权限，将刚才新建的人员添加到新建的角色里，角色权限选择所有。

图15 新建角色
  ![新建角色](./document/readme-image-015.jpg)
图16 新建角色编辑
  ![新建角色编辑](./document/readme-image-016.jpg)
步骤 3	使用新建的人员账户登录系统，详细使用请参考“在线帮助”。

图17 门户管理
  ![门户管理](./document/readme-image-017.jpg)

