#!/bin/bash
wget -P /opt/ https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/rapid-deployment-of-oa-system-based-on-lanling-mk-paas/open-source-software/mk-paas.tar.gz
mkdir /opt/MKPaaS
mkfs.xfs /dev/vdb
cat >> /etc/fstab << EOF
/dev/vdb             /opt/MKPaaS          xfs    defaults        0 0 
EOF
mount -a
cd /opt
tar -xf mk-paas.tar.gz
tar -xf MKPaaS.tar.gz
tar -xf mk-8722a*.tar.gz
mv MKPaaS-app/* MKPaaS/MKPaaS-app
mkdir /root/attaches/
chmod 666 /root/attaches/
cp -a /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak
wget -O /etc/yum.repos.d/CentOS-Base.repo https://repo.huaweicloud.com/repository/conf/CentOS-7-reg.repo
sed -i 's/\(mkpaas-console-config_servers_main-root=\).*/\1'$1'/' /opt/MKPaaS/MKPaaS-console/config.properties
sed -i 's/\(mkpaas-console-config_servers_node01-host=\).*/\1127.0.0.1/' /opt/MKPaaS/MKPaaS-console/config.properties
sed -i 's/\(mkpaas-console-config_servers_node01-root=\).*/\1'$1'/' /opt/MKPaaS/MKPaaS-console/config.properties
sed -i 's/\(mkpaas-console-config_apps_mk-apps=\).*/\1discovery-center, gateway-server, base-server, app-server, convert-server, bpm-server, monitor-server/' /opt/MKPaaS/MKPaaS-console/config.properties
sed -i 's/\(mkpaas-console-config_arranges_node01-apps=\).*/\1system_init, install_jdk, redis, mysql, nginx, discovery-center, gateway-server, base-server, app-server, convert-server, bpm-server, monitor-server/' /opt/MKPaaS/MKPaaS-console/config.properties
ETH0=$(hostname -I|awk '{print $1}')
sed -i 's/\(mk-paas-config_discovery-center_config_app-config_global.properties_kmss.sys.backend.dns=\).*/\1http:\/\/'$ETH0':8088/' /opt/MKPaaS/MKPaaS-console/config.properties
cat >>  /opt/MKPaaS/MKPaaS-console/config.properties <<- EOF
mk-paas-config_gateway-server_config_bootstrap.properties_spring.cloud.config.uri=http://127.0.0.1:9100/config/
mk-paas-config_base-server_config_bootstrap.properties_spring.cloud.config.uri=http://127.0.0.1:9100/config/
mk-paas-config_app-server_config_bootstrap.properties_spring.cloud.config.uri=http://127.0.0.1:9100/config/
mk-paas-config_convert-server_config_bootstrap.properties_spring.cloud.config.uri=http://127.0.0.1:9100/config/
mk-paas-config_bpm-server_config_bootstrap.properties_spring.cloud.config.uri=http://127.0.0.1:9100/config/
mk-paas-config_monitor-server_config_bootstrap.properties_spring.cloud.config.uri=http://127.0.0.1:9100/config/
EOF
yum install expect python-virtualenv  -y
cd /opt/MKPaaS/MKPaaS-console
sh update_config.sh
expect << EOF 
set timeout -1
cd /opt/MKPaaS/MKPaaS-console
spawn sh start.sh  
expect {  
 "请输入序号:"  
   {  
    send "1\n"  
    expect "请输入序号*"  { send "1\n"}
    expect "请输入序号*"  { send "0\n"}
    expect "请输入序号*"  { send "0\n"}
    expect "eof" { exit }
   }  
}  
EOF
su - mkpaas -c "cd /opt/MKPaaS/install/discovery-center/bin/;sh shutdown.sh"
su - mkpaas -c "cd /opt/MKPaaS/install/gateway-server/bin/;sh shutdown.sh"
su - mkpaas -c "cd /opt/MKPaaS/install/monitor-server/bin/;sh shutdown.sh"
su - mkpaas -c "cd /opt/MKPaaS/install/app-server/bin/;sh shutdown.sh"
su - mkpaas -c "cd /opt/MKPaaS/install/bpm-server/bin/;sh shutdown.sh"
su - mkpaas -c "cd /opt/MKPaaS/install/convert-server/bin/;sh shutdown.sh"
su - mkpaas -c "cd /opt/MKPaaS/install/base-server/bin/;sh shutdown.sh"
su - mkpaas -c "cd /opt/MKPaaS/install/discovery-center/bin/;sh run.sh"
while :
do  
    sleep 10
    echo "wait......"
    if netstat -nltp|grep -q "9100"
    then
      echo "discovery-center安装成功"
      su - mkpaas -c "cd /opt/MKPaaS/install/gateway-server/bin/;sh run.sh"
      break
    fi  
done
while :
do  
    sleep 10
    echo "wait......"
    if netstat -nltp|grep -q "9101"
    then
      echo "gateway-server安装成功"
      su - mkpaas -c "cd /opt/MKPaaS/install/base-server/bin/;sh run.sh"
      break
    fi  
done
while :
do  
    sleep 10
    echo "wait......"
    if netstat -nltp|grep -q "9104"
    then
      echo "base-server安装成功"
      su - mkpaas -c "cd /opt/MKPaaS/install/monitor-server/bin/;sh run.sh"
      su - mkpaas -c "cd /opt/MKPaaS/install/app-server/bin/;sh run.sh"
      su - mkpaas -c "cd /opt/MKPaaS/install/bpm-server/bin/;sh run.sh"
      su - mkpaas -c "cd /opt/MKPaaS/install/convert-server/bin/;sh run.sh"
      break
    fi  
done